#@PydevCodeAnalysisIgnore
from pyparsing import delimitedList,Literal,Keyword,Regex,ZeroOrMore,Suppress,Optional,QuotedString,Word,hexnums,alphas,\
    dblQuotedString,FollowedBy, sglQuotedString,oneOf,Group,Forward,And,\
    unicodeString,nums,commaSeparatedList

LPAREN = Literal("(")
RPAREN = Literal(")")
COMMA = Literal(",")


Float = Regex(r"[0-9]?[1-9]*\.[0-9]")
Int = Regex(r"[1-9][0-9]*")

funcName = Regex(r"([a-z])+([a-zA-Z0-9_]*)")

phpNum = Optional("-") + (Float | Int) 

dblQuotedString.setParseAction(lambda t:t[0][1:-1])

phpVarName = Regex(r"\$([a-z])+([a-zA-Z0-9_]*)")
print phpVarName.parseString("""$a3333_dSADFFfdf3""")

lassignment = phpVarName + Literal("=")
print lassignment.parseString("$dave=")

stringVar = Forward()
stringVar << (dblQuotedString + ZeroOrMore(Literal(".") + (phpVarName | stringVar)))  
assignment = Forward()
assignment << lassignment + Group(stringVar| phpNum)

#print commaSeparatedList.parseString(""",$dog,$cat,moose,,""")

funcall = Forward()
funcallList = Forward()
funcall << funcName + LPAREN + Optional((funcall |stringVar| phpVarName)) + Optional( funcallList)  +RPAREN
funcallList << COMMA + (funcall |stringVar| phpVarName)
print funcall.parseString("""dave(dave(ted()))""")
print funcall.parseString("""dave(dave(ted("word!")))""")
print funcall.parseString("""dave(dave(ted("$word!")))""")
print funcall.parseString("""dave(dave(ted($word,$dog)))""")
print funcall.parseString("""dave(dave(ted($word,lucy())))""")

#print funcall.parseString("""dave(dave( ted() joe() ))""")
 

print assignment.parseString("""$dave = "asdf" """)
print assignment.parseString("""$dave = "asdf"  . "dso" """)
print assignment.parseString("""$dave = "asdf" . $poo  . "dso" """)
print assignment.parseString("""$sql = "INSERT INTO BLAH(id) VALUE(" . $uid  . ")" """)
print assignment.parseString("""$sql = "INSERT INTO BLAH(id) VALUE(" . $uid  . ")" """)
print assignment.parseString("""$counter = 10 """)
print assignment.parseString("""$counter = 0.5 """)
print assignment.parseString("""$counter = - 0.5 """)
print assignment.parseString("""$counter = -0.5 """)
print assignment.parseString("""$counter = -10 """)
print assignment.parseString("""$counter = 10 """)
#should fail
#print assignment.parseString("""$counter = 01 """)
